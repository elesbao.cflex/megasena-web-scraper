const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

//const fs = require('fs');
const cors = require('cors');

// Resolvendo o CORS em desenvolvimento
app.use((req, res, next) => {
	//Qual site tem permissão de realizar a conexão, no exemplo abaixo está o "*" indicando que qualquer site pode fazer a conexão
    res.header("Access-Control-Allow-Origin", "*");
	//Quais são os métodos que a conexão pode realizar na API
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    app.use(cors());
    next();
});

/* Server AWS
	IP address: 54.207.244.11
*/

// Database MongoDB information
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb+srv://workshop:railmp123@cluster0.l5spr.mongodb.net/?retryWrites=true&w=majority";
const myDatabase = "megasena";

// Webscrapping
const rp = require('request-promise')
const cheerio = require('cheerio')

const options = {
	uri: 'https://www.mazusoft.com.br/mega/resultado.php?concurso=',
	transform: function (body) {
		return cheerio.load(body)
	}
}

// Empty route to check if the server is up
app.get('/', (req, res) => {
	
	res.send("Yuuki-the-cat web scraper v2.0.1. Server is running.");
	console.log("Yuuki-the-cat web scraper v2.0.1. Server is running.");

 }); //end of 'empty route'
 
// Reading controles
app.get('/controles', (req, res) => {

    //Lendo collection 'controles'
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db(myDatabase);
        dbo.collection("controles").find({}).toArray(function(err, result) {
          if (err) throw err;

		  console.log("*** Controles ***");
          console.log(result);
          res.json(result); //enviando como objeto JSON
          db.close();
        });
      });

 }); //end of leitura 'controles'

// Reading megasena regular
app.get('/megasena-regular', (req, res) => {

    //Lendo collection 'jogosRegulares'
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db(myDatabase);
        dbo.collection("jogosRegulares").find({}).toArray(function(err, result) {
          if (err) throw err;

		  console.log("*** Jogos Regulares ***");
          console.log(result);
          res.json(result); //enviando como objeto JSON
          db.close();
        });
      });

 }); //end of leitura 'megasena-regular'
 
// Reading megasena extra
app.get('/megasena-extra', (req, res) => {

    //Lendo collection 'jogosExtras'
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db(myDatabase);
        dbo.collection("jogosExtras").find({}).toArray(function(err, result) {
          if (err) throw err;

		  console.log("*** Jogos Extras ***");
          console.log(result);
          res.json(result); //enviando como objeto JSON
          db.close();
        });
      });

 }); //end of leitura 'megasena-extra'
 

// #################################
// This server will continuously run
// #################################

// Defining global variables
var loop_counter = 0;
var Concurso_anterior = "2618";
var Data_anterior = "05/08/2023";
var Data_warning = "05/08/2023";
var alarme;
var threshold;
var destinatario = [];

var flagJogoAtivo = [];
var cartaoExtra = [];
var cartaoRegular = [];
var cartoesJogados = [];

// Reading database MongoDB
const createClock_Controles = setInterval(lerControles, 21600000); // Time is milliseconds
const createClock_JogosRegulares = setInterval(lerJogosRegulares, 21600000); // Time is milliseconds
const createClock_JogosExtras = setInterval(lerJogosExtras, 21600000); // Time is milliseconds

// Calling function 'ytc_megasena()' by interval
const createClock_YTC = setInterval(ytc_megasena, 14400000); // Time is milliseconds
/* setInterval() values:
	 3600000 milliseconds:  1 hour
	14400000 milliseconds:  4 hours
	21600000 milliseconds:  6 hours
	28800000 milliseconds:  8 hours
	43200000 milliseconds: 12 hours
	86400000 milliseconds: 24 hours
*/

// Function to scrape targeted web page
function ytc_megasena() {
	
	// Reading MongoDB
/*	
	console.log("**********");
	console.log("lerControlles");
	console.log("Concurso_anterior: "+Concurso_anterior);
	console.log("Data_anterior: "+Data_anterior);
	console.log("Data_warning: "+Data_warning);
	console.log("loop_counter: "+loop_counter);
	console.log("threshold: "+threshold);
	console.log("destinatario: ");
	console.log(destinatario);
/*	
	console.log("**********");
	console.log("lerJogosRegulares");
	for(i = 0; i < cartaoRegular.length; i++) {
		console.log(cartaoRegular[i]);
	}
	
	console.log("**********");	
	console.log("lerJogosExtras");
	for(i = 0; i < cartaoExtra.length; i++) {
		console.log(cartaoExtra[i]+","+flagJogoAtivo[i]);
	}
	console.log("**********");
*/

	// Increment 'loop_counter' cycle
	loop_counter++;
	
	// Logging server information: getting and transforming current date
	var today = new Date();
	
	var dia_today = today.getDate();
	var mes_today = parseInt(today.getMonth())+1;
	var ano_today = today.getFullYear();
	
	// Assembling the current date ('dd/mm/aaaa')
	var Data_corrente = dia_today+"/"+mes_today+"/"+ano_today;
	
	// Getting the day of the week
	var dia_semana = getDiaSemana(today);
	
	// Adding 'dia_semana' to 'today' variable
	today = today.toLocaleString('pt-BR', { timeZone: 'America/Sao_Paulo' });
	today = dia_semana+", "+today;	
	
	// Writing 'texto_default'	
	var server_days_up = diffEntreDatas(); // Calculates number of days the server is up
	var texto_default = "\nServer is running for "+server_days_up+" days. Cycle number: "+loop_counter
						+"\n"+today
						+"\nResultados da Mega Sena | Mazusoft"
						+"\nÚltimo concurso: "+Concurso_anterior+", Data: "+Data_anterior;
						
	console.log(texto_default);

	// Webscraping -- geting information from target webpage
	rp(options)
		.then(($) => {;
	  	
	    var wp_content = $.text(); // Get the webpage information as text
	    wp_content = wp_content.replace(/\r?\n|\r|\t/g, '');  // Eliminating special characters (carriage return, newline, tab)
	    
	    //console.log("--------------");
	    //console.log(wp_content);
	    //console.log("--------------");
	    
	    // Extracting variables from the web page
	    var indice_1, indice_2; // Variables to store indexes for searching
	    var mega_acumulada = "R$ 0,00";
	    
		indice_1 = wp_content.indexOf("Resultado da MegaSena");	// Seeking for the beginning of 'Number_concurso' in wp_content text
		var Numero_concurso = wp_content.substr(indice_1+22, 4);
		var Data_concurso = wp_content.substr(indice_1+30, 10);
		
		indice_1 = wp_content.indexOf("Números Sorteados");	// Seeking for the beginning of 'Sena_concurso' in wp_content text
		var Sena_concurso = wp_content.substr(indice_1+17, 18);
		
		indice_1 = wp_content.indexOf("6 Acertos");	// Seeking for the megasena winners in wp_content text
		indice_2 = wp_content.indexOf("R$", indice_1);
		var seis_acertos = wp_content.substring(indice_1+9, indice_2);
		indice_2 = wp_content.indexOf("5 Acertos", indice_1);
		var valor_sena = wp_content.substring(indice_1+9+seis_acertos.length, indice_2);
		
		indice_1 = wp_content.indexOf("5 Acertos");	// Seeking for the megasena winners in wp_content text
		indice_2 = wp_content.indexOf("R$", indice_1);
		var cinco_acertos = wp_content.substring(indice_1+9, indice_2);
		indice_2 = wp_content.indexOf("4 Acertos", indice_1);
		var valor_quina = wp_content.substring(indice_1+9+cinco_acertos.length, indice_2);
		
		indice_1 = wp_content.indexOf("4 Acertos");	// Seeking for the megasena winners in wp_content text
		indice_2 = wp_content.indexOf("R$", indice_1);
		var quatro_acertos = wp_content.substring(indice_1+9, indice_2);
		indice_2 = wp_content.indexOf("Sorteio", indice_1);
		
		// Checking if 'rateio vencedores' has already been calculated
		if (quatro_acertos == "0") {
			
			console.log("*** Ciclo "+loop_counter+" abortado! Rateio vencedores sendo calculado.")
			return;
			
		}
		
		// Getting the correct 'valor_quadra'
		var valor_quadra = wp_content.substring(indice_1+9+quatro_acertos.length, indice_2);
				
		// Checking for accumulated megasena
		if (seis_acertos == "0") {
			
			indice_1 = wp_content.indexOf("MEGA-SENA** ACUMULOU **");	// Seeking for 'megasena acumulada' value
			indice_2 = wp_content.indexOf(",", indice_1);
			mega_acumulada = wp_content.substring(indice_1+23, indice_2)+",00";			
			
		}
		
		// Printing information in the console
		//console.log("\n");
		//console.log("Concurso atual: "+Numero_concurso, "Data: "+Data_concurso);
		console.log("Números sorteados: "+Sena_concurso);		
		console.log("6 Acertos: "+seis_acertos+", valor prêmio: "+valor_sena);
		console.log("5 Acertos: "+cinco_acertos+", valor prêmio: "+valor_quina);
		console.log("4 Acertos: "+quatro_acertos+", valor prêmio: "+valor_quadra);	
		console.log("** Megasena acumulada: "+mega_acumulada);		
		
		// Checking Megasena results
		var texto_email = checkMegasena(Numero_concurso, Data_concurso, Sena_concurso);
		
		// Sending 'texto_email' to email
		var titulo_email = "Yuuki-the-cat informa resultado megasena: concurso "+Numero_concurso+", data "+Data_concurso;
		
		// Filling 'destinatario_email' from MongoDB
		var destinatario_email = destinatario[0];
				
		for(i = 1; i < destinatario.length; i++) {
						
			destinatario_email = destinatario_email+","+destinatario[i];
			
		}
				
		/* Valid recipient list:
			"elesbao.oliveira@railmp.com,tatiana.vaz.dso@gmail.com,marilia.vsoliveira@gmail.com,helenavso@gmail.com"
			"elesbao.oliveira@railmp.com,tatiana.vaz.dso@gmail.com"
			"elesbao.oliveira@railmp.com,elesbao@cflex.com.br"
			"elesbao.oliveira@railmp.com"
		*/
		
		// Preparing email warnings
		
		// Checking for missing megasena concurso
		if (parseInt(Numero_concurso) - parseInt(Concurso_anterior) > 1) {

			var destinatario_missing = "elesbao.oliveira@railmp.com";
			var titulo_missing = "Yuuki-the-cat server is running: missing last megasena game";
			var texto_missing = texto_default+"\n\n*** WARNING: Missing last megasena game !!! ***";
			
			// Sending email to recipients: case 'missing game'
			sendEmail(titulo_missing, texto_missing, destinatario_missing);		
						
		}
		
		// Checking if server is running
		if (Numero_concurso == Concurso_anterior && Data_warning != Data_corrente) {
			
			var destinatario_running = "elesbao.oliveira@railmp.com";
			var titulo_running = "Yuuki-the-cat server is running: no new megasena game";
			
			// Sending email to recipients: case 'server is running'
			sendEmail(titulo_running, texto_default, destinatario_running);
			Data_warning = Data_corrente;			
			
		}
		
		// Checking for new megasena Game
		if (Numero_concurso != Concurso_anterior) {

			// Printing 'Resultados do concurso' and final credits
			texto_email = texto_email 	+ "\n\n*** Resultados do concurso *** "
										+ "\n6 Acertos: "+seis_acertos+", valor prêmio: "+valor_sena
										+ "\n5 Acertos: "+cinco_acertos+", valor prêmio: "+valor_quina
										+ "\n4 Acertos: "+quatro_acertos+", valor prêmio: "+valor_quadra;
										
			// Printing 'Megasena acumulada' whenever is the case
			if(seis_acertos == "0") {
				
				texto_email = texto_email 	+ "\n** Megasena acumulada: "+mega_acumulada;
				
			}
			
			// Printing final credits
			texto_email = texto_email	+"\n\nyuuki-the-cat web scraper v2.0.1"
										+"\nFonte: https://www.mazusoft.com.br/mega/resultado.php?concurso="+Numero_concurso;
					
			// Sending email to recipients: case 'new megasena game'
			sendEmail(titulo_email, texto_email, destinatario_email);
			Concurso_anterior = Numero_concurso;
			Data_anterior = Data_concurso;	
			Data_warning = Data_corrente;

		}
		

			// Updating database 'controles'
/*
setTimeout(() => {
			MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  var dbo = db.db("myDatabase");
			  var myquery = { versao: "2.0.1" };
			  var newvalues = { $set: {Concurso_anterior: Concurso_anterior, Data_anterior: Data_anterior, Data_warning: Data_warning, loop_counter: loop_counter } };
			  dbo.collection("controles").updateOne(myquery, newvalues, function(err, res) {
			    if (err) throw err;
			    console.log("Collection 'controles' is updtated.");
			    db.close();
			  });
			});
}, 2000);
*/		
				    
		}) // End of '.then()'
	
  		.catch((err) => {
			console.log(err);
		})

} // End of function'ytc_megasena()'

// #######################################
// Function to check the Megasena result
function checkMegasena(get_Concurso, get_Data, get_Sena) {
		
	// Data structure for Megasena results
	var Numero_concurso = get_Concurso; // Receiving 'Numero_concurso'
	var Data_concurso = get_Data;		// Receiving 'Data_concurso'
	var Bola_concurso = new Array();
	
	// Extracting data from 'get_Sena'
	Bola_concurso.push(get_Sena.substr(0,2));
	Bola_concurso.push(get_Sena.substr(3,2));
	Bola_concurso.push(get_Sena.substr(6,2));
	Bola_concurso.push(get_Sena.substr(9,2));
	Bola_concurso.push(get_Sena.substr(12,2));
	Bola_concurso.push(get_Sena.substr(15,2));
	
	// Data structure for Megasena cards
	var Jogo_analisado = new Array();
	var Numero_acertos = new Array();
	
	// Control variables
	var counter = 0;
	var texto_email = "\nYuuki-the-cat informa resultados da Megasena.";
	
	// Filling 'cartoesJogados'
	cartoesJogados = [];
	cartoesJogados = cartaoRegular.slice(0);
	
	// Checking for 'flagJogoAtivo'		
	for(i = 0; i < cartaoExtra.length; i++) {
		
		if(flagJogoAtivo[i] == true) {			
			cartoesJogados.push(cartaoExtra[i]);			
		}
		
	}
		
	
	//console.log("***** cartoesJogados ******");
	//console.log(cartoesJogados);
	
	
	// Checking if it is the last game of 'Teimosinha'
	alarme = Numero_concurso.valueOf();
	alarme = alarme % 8;
	
	if (alarme == threshold) {		
		texto_email = texto_email	+"\n\n************ ATENÇÃO *************"
									+"\nEste é o último jogo da Teimosinha\nConcurso: "
									+Numero_concurso+", Data: "+Data_concurso
									+"\n**********************************\n";		
	}
		
	// Compare results for each Megasena	
			
	// Printing 'Numero_concurso' and result
	texto_email = texto_email	+"\nConcurso: "+Numero_concurso+", Data: "+Data_concurso
								+"\nNúmeros sorteados ["+Bola_concurso[0]+", "+Bola_concurso[1]+", "+Bola_concurso[2]+", "
								+Bola_concurso[3]+", "+Bola_concurso[4]+", "+Bola_concurso[5]+"]";

	// Loop to controlling for number of 'Cartao'
	for (var j = 0; j < cartoesJogados.length; j++) {
		
		// Clearing variables
		while(Jogo_analisado.length) {
        	Jogo_analisado.pop();
        }
        
		while(Numero_acertos.length) {
        	Numero_acertos.pop();
        }
        
		counter = 0;

		// Filling 'Jogo_analisado'
		Jogo_analisado = cartoesJogados[j].slice();
		//console.log("***** Jogo_analisado ******");
		//console.log(Jogo_analisado);
		
		// Writing Megasena game (bet)
		var jogo = j+1;
		var jogo_str;
		
		// Changing 'jogo' into 2-digits string
		if (jogo < 10) {
			
			jogo_str = "J0"+jogo.toString();
			
		} else {
			
			jogo_str = "J"+jogo.toString();
			
		}
		
		// Printing bet cards
		texto_email = texto_email+"\n"+ jogo_str +" ["+Jogo_analisado+"]";			
						
		// Loop for each game
		for (var k = 0; k < Jogo_analisado.length; k++) {
			
			var str1 = Jogo_analisado[k];				
			
			// Loop for 'Bola_concurso'
			for (var i = 0; i < Bola_concurso.length; i++) {
				
				var str2 = Bola_concurso[i];
									
				if (str1 == str2) {				
					Numero_acertos.push(str1);
					counter++;				
				}
				
			} // End of 'Bola_concurso' loop				
			
		} // End of 'game' loop
		
		// Find Megasena results
		switch (counter) {			
			case 0:				
				// Print result
				texto_email = texto_email+"\t> Nada ";
				break;
				
			case 1:				
				// Print result
				texto_email = texto_email+"\t> Ás ["+Numero_acertos+"]";
				break;
				
			case 2:				
				// Print result
				texto_email = texto_email+"\t> Duque ["+Numero_acertos+"]";
				break;
				
			case 3:				
				// Print result
				texto_email = texto_email+"\t> Terno ["+Numero_acertos+"]";
				break;
				
			case 4:				
				// Print result
				texto_email = texto_email+"\t> QUADRA ["+Numero_acertos+"]";
				break;
				
			case 5:				
				// Print result
				texto_email = texto_email+"\t> QUINA ["+Numero_acertos+"]";
				break;
				
			case 6:				
				// Print result
				texto_email = texto_email+"\t> SENA \\O/ *** Ficou milionário !!! ***";
				break;				
			}
		
	} // End of 'Cartao' control loop
	
	console.log(texto_email);
	return texto_email;
		
 } // End of function 'checkMegasena()'
 
// #######################################
// Function to send 'texto_email' to email
function sendEmail(get_Titulo, get_Texto, get_Destinatario) {
	
	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
		user: 'elesbao@cflex.com.br',
		pass: 'R@ilmp123'
		}
	});

	var mailOptions = {
		from: 'elesbao@cflex.com.br',
		to: get_Destinatario,
		//cc: 'elesbao@cflex.com.br',
		//bcc: 'elesbao@cflex.com.br',
		subject: get_Titulo,
		text: get_Texto
	};

	transporter.sendMail(mailOptions, function(error, info){
		if (error) {
			console.log(error);
		} else {
			console.log('Email sent: ' + info.response);
			res.send('Email enviado com sucesso.');
		}
	});
	
}

// #######################################
// Function to calculate the difference between 2 dates
	function diffEntreDatas() {
		
	const now = new Date(); // Data de hoje
	const past = new Date('2022-10-28'); // Outra data no passado
	const diff = Math.abs(now.getTime() - past.getTime()); // Subtrai uma data pela outra
	const days = Math.ceil(diff / (1000 * 60 * 60 * 24)); // Divide o total pelo total de milisegundos correspondentes a 1 dia. (1000 milisegundos = 1 segundo).
	
	return days;
	
	}

// #######################################
// Function to find the day of the week
	function getDiaSemana(data_recebida) {
		
	    var week_options = { weekday: 'long'};
	    var weekday = new Intl.DateTimeFormat('pt-BR', week_options).format(data_recebida); // domingo, segunda-feira, terça-feira, etc.
	    return weekday;
	}
	
// #######################################
// Reading database 'controles'
function lerControles() {
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db(myDatabase);
	  dbo.collection("controles").find({}).toArray(function(err, result) {
	    if (err) throw err;
	    
	    //console.log("*** Find ***");
	    //console.log("result.length is: "+result.length);
	    //console.log(result);    
	    
	    // Filling records	    
	    //Concurso_anterior = result[0].Concurso_anterior;
	    //Data_anterior = result[0].Data_anterior;
	    //Data_warning = result[0].Data_warning;
	    //loop_counter = result[0].loop_counter;
	    threshold = result[0].threshold;
	    destinatario = result[0].destinatario;
	    
	    db.close();

		// Fechando servidor após leitura do último registro
		//process.exit();
	  
	  });
	}); // end of connect.find
} // end of 'lerControles'

// #######################################
// Reading database 'jogosRegulares
function lerJogosRegulares() {
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db(myDatabase);
	  dbo.collection("jogosRegulares").find({}).toArray(function(err, result) {
	    if (err) throw err;
	    
	    //console.log("*** Find ***");
	    //console.log("result.length is: "+result.length);
	    //console.log(result);   
	    
		// Filling records
		cartaoRegular = [];
		for (i = 0; i < result.length; i++) {
			
			cartaoRegular[i] = result[i].Jogo_regular;
			
		}
	    	    
	    db.close();

		// Fechando servidor após leitura do último registro
		//process.exit();
	  
	  });
	}); // end of connect.find
} // end of 'lerJogosRegulares'

// #######################################
// Reading database 'jogos_Extras'
function lerJogosExtras() {
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var dbo = db.db(myDatabase);
	  dbo.collection("jogosExtras").find({}).toArray(function(err, result) {
	    if (err) throw err;
	    
	    //console.log("*** Find ***");
	    //console.log("result.length is: "+result.length);
	    //console.log(result);   
	    
		// Filling records
		cartaoExtra = [];
		for (i = 0; i < result.length; i++) {
			
			flagJogoAtivo[i] = result[i].flagJogoAtivo;
			cartaoExtra[i] = result[i].Jogo_extra;
			
		}
	    
	    db.close();

		// Fechando servidor após leitura do último registro
		//setTimeout(() => {
			//process.exit();
		//}, 1000);
	  
	  });
	}); // end of connect.find
} // end of 'lerJogosExtras'

/* ############# End of Server Nodejs program ############

Samples of sync and async delays

>>> ASYNC delay <<<

setTimeout(function(){
	your code will wait for 5 seconds to run
},5000);


>>> SYNC delay <<<

--- chamada função syncDelay()
console.log("Before the delay");
syncDelay(5000);
console.log("After the delay");	
	
--- Sync delay function
function syncDelay(milliseconds){
	var start = new Date().getTime(); // milliseconds
	var end=0;
	while( (end-start) < milliseconds){
		end = new Date().getTime();
	}
}
	
*/

app.listen(3001, () => console.log('Started server at http://localhost:3001!'));
